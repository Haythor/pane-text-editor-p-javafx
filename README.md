# Pane d'édition de texte

![](res/icon.png)

## Objectif de l'application

> Concevoir un *pane (javaFX)* qui a les fonctionnalités standard d'un éditeur de texte ainsi que des options de coloration des caractères.

Voici donc les principales fonctions que l'on pourra retrouver :
- Un curseur pour l'édition.
- Raccourcies clavier d'édition, de déplacement,...
- Différentes options de contrôle à la souris.
- Visibilité des numéro de la ligne.
- Capacités de modification du style (couleur).
- Différentes options pour obtenir le texte (tout ou lignes à lignes)
- Différentes options afin d'ajouter du texte (à la suite)

## Le curseur d'édition

> Comme dans toutes application d'édition de texte, le curseur permet de renseigner la position ou il va éditer : cela est donc primordiale.

- Graphiquement il s'agit d'une simple barre verticale de la hauteur
  d'une ligne et de 1px d'épaisseur qui se retrouve juste après le
  caractère courant.
  Pour éviter les effets d'opacités réduites liées aux position à virgules flottantes le curseur est *snap* au pixel suivant.
- Une fonction permettra à un *class* externe de replacer le curseur (après l'ajout de texte par exemple).

## Les raccourcies clavier

> On peut différencier les raccourcies clavier généraux et ceux plus spécifique plus utiliser par les développeurs.
> (Ici le mot block est utilisée pour désigner la ligne courante ou les lignes relative à une sélection)

- Cas généraux :
  - Le déplacement avec les **flèches**
  - Retour à la ligne avec **entrer**
  - Supprimer avec **BACK_SPACE** ou **DELETE**
  - Tout sélectionner **ctrl+A** 
  - Copier **ctrl+C**, coller **ctrl+V**, couper **ctrl+X**
  - Désélectionner **ctrl+D**
  - Allez au début de la ligne **HOME**
  - Allez à la fin de la ligne **END**
- Cas plus spécifique :
  - Suppression de block **ctrl+shift+K**
  - Déplacement de block (haut/bas) **alt+flèches**
  - Duplication de block (haut/bas) **alt+shift+flèches**
  - Suppression du mot précédant **ctrl+BACK_SPACE**
  - Suppression du mot suivant **ctrl+DELETE**
  - Allez au mot précédant **ctrl+LEFT**
  - Allez au mot suivant **ctrl+RIGHT**
  - Augmenter la sélection de un caractère vers la gauche **shift+LEFT**
  - Augmenter la sélection de un caractère vers la droite **shift+RIGHT**
  - Augmenter la sélection de un mot vers la gauche **ctrl+shift+LEFT**
  - Augmenter la sélection de un mot vers la droite **ctrl+shift+RIGHT**

## Les contrôles souris

Le clique permet de placer le curseur en fin de ligne ou entre plusieurs caractères.
Le cliquer déplacer permet de faire une sélection entre le point du clique et le point de relâchement.
Il est également possible de cliquer sur le numéro de ligne pour placer le curseur au début de celle-ci.

## Aspect graphique

### Les thèmes de l'interface

> Le thème par défaut est le thème sombre ; il comporte également des nuances bleu.

![](res/theme.dark.png)

Possibilités de switcher vers un thèmes clair par défaut.

![](res/theme.light.png)

Possibilités de créer un nouveau thème en choisissant les couleurs suivantes :
 - Fond du viewer texte
 - Fond pane numéro de ligne
 - Du texte par défaut (en dehors des règles de coloration)
 - Texte des numéros de ligne normal
 - Texte des numéros de ligne sélectionné
 - Curseur
 - Sélection
 - Type de BlendMode de la sélection

### La coloration de texte

> Capacité de l'éditeur à gérer la coloration de mots et de block définit par l'utilisateur.

1. Il est possible de définir un mot qui est constituer d'une chaine de caractères et d'une couleur (en hexadécimale).
   Ne prend pas en charge des délimiteurs : le mots sera coloré dans toutes les situations (hormis une recoloration par une règles à priorité plus forte).
   Si un mot est contenue dans un autre, c'est le dernier ajouter dans les paramètres qui aura la priorité.

2. On peut également définir des règles de coloration de block avec une chaine de délimitation de début et une autre de fin et la couleur.
   La coloration de block est effectuer après celle des mots : si le block contient des mots ils seront colorés par le celui-ci.
   La détection se fait de telle manière que si un pattern de début est trouvé, les suivants sont ignorés tant que le pattern de fin n'est pas trouver.

## L'historique

> Il s'agit de pouvoir établir un historique de la plupart des actions pour quels ne soit pas irréversible.

Globalement les ajouts de texte et les suppressions peuvent être annulé. Cependant des évènements comme les sélections ne seront pas restaurer.

Liste des actions pouvant être annulé :
- Ajout de caractère
- Suppression de caractère
- Collage
- Suppression du texte de sélection
- Touche entrer
- Duplication de block
- Déplacement de block
- L'annulation (*Redo*)

## Les principale méthodes de **TextEditor**

```java
TextEditor textEditor = new TextEditor();
```

### Thème et coloration

- Paramétrage d'un thème par défaut :
  ```java
  textEditor.setTheme(Theme.DARK);
  ```
  Ou par le constructeur directement :
  ```java
  TextEditor textEditor = new TextEditor(Theme.LIGHT);
  ``` 

- Ajout d'une règle de coloration de mots :
  ```java
  textEditor.addWordColor("Hello ", "47CC47");
  ```

- Ajout d'une règle de coloration de block :
  ```java
  textEditor.addBlockColor("/*", "*/", "919191");
  ```

### Interactions et lecture  contenue

- Ajouts de texte à la suite de la position actuel du curseur :
  ```java
  textEditor.appendtextToCursor(" mon super texte, ");
  ```

- Ajouts de texte à la suite du texte courant :
  ```java
  textEditor.appendText("Chaine de caractères\n");
  ```

- Récupération de l'ensemble du texte du *pane* :
  ```java
  textEditor.getAllText();
  ```

- Suppression de l'ensemble du texte (et de historique) :
  ```java
  textEditor.clearText();
  ```