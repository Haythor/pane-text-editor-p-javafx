/*
 * Test.java                     date: 28/01/2020 10:30
 * IUT info1 2019/2020 groupe 3, pas de droits d'auteur
 */

package org.unrealview.test;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.unrealview.vue.TextEditor;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class Test extends Application {

	private TextEditor te1 = new TextEditor();
	private TextEditor te2 = new TextEditor();

	public static void main(String[] args) { launch(); }

	@Override
	public void start(Stage stage) {

		// SplitPane splitPane = new SplitPane(te1, te2);
		BorderPane borderPane = new BorderPane();
		borderPane.setCenter(te2);

		te1.addRegexColor("\\bpain\\b", Color.valueOf("F05D43"));
		te1.addRegexColor("\\/\\*[^*]*\\*+([^\\/][^*]*\\*+)*\\/",
		                  Color.valueOf("65ABFF"));
		te1.addRegexColor("(?<=\\* )@param\\b", Color.valueOf("FFCE35"));

		te2.addRegexColor("\\bLEVER\\b", Color.valueOf("50B4E6"));
		te2.addRegexColor("\\bBAISSER\\b", Color.valueOf("50B4E6"));
		te2.addRegexColor("\\bCENTRER\\b", Color.valueOf("50E6BB"));
		te2.addRegexColor("\\bRAZ1\\b", Color.valueOf("E65050"));
		te2.addRegexColor("\\bRAZ2\\b", Color.valueOf("E65050"));
		te2.addRegexColor("\\bINCREMENTER1\\b", Color.valueOf("E65073"));
		te2.addRegexColor("\\bINCREMENTER2\\b", Color.valueOf("E65073"));
		te2.addRegexColor("\\bINCREMENTER3\\b", Color.valueOf("E65073"));
		te2.addRegexColor("\\bNORD\\b", Color.valueOf("E6A750"));
		te2.addRegexColor("\\bSUD\\b", Color.valueOf("E6A750"));
		te2.addRegexColor("\\bEST\\b", Color.valueOf("E6A750"));
		te2.addRegexColor("\\bOUEST\\b", Color.valueOf("E6A750"));
		te2.addRegexColor("\\(\\*[^*]*\\*+([^\\)][^*]*\\*+)*\\)",
		                  Color.valueOf("919191"));
		te2.addRegexColor("\\#STOP\\b", Color.valueOf("E60F00"));

		Scene scene = new Scene(borderPane, 1000, 600);

		stage.setScene(scene);
		stage.setTitle("Notepad");
		// stage.getIcons().add(new Image("compose.png"));
		stage.show();
	}
}