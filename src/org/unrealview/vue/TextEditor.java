/*
 * TextEditor.java               date: 28/01/2020 10:36
 * IUT info1 2019/2020 groupe 3, pas de droits d'auteur
 */

package org.unrealview.vue;

import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.util.Duration;
import org.unrealview.model.Caret;
import org.unrealview.model.coloration.Painter;
import org.unrealview.model.coloration.RegexColor;
import org.unrealview.model.history.History;
import org.unrealview.model.text.TextTools;
import org.unrealview.vue.scroll.CustomScrollBar;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class TextEditor extends StackPane {

	private BorderPane root;

	private Rectangle activeLineText;
	private Rectangle activeLineTextFit;
	private Rectangle activeLineNumber;

	private BorderPane      textPane;
	private TextFlow        textFlow;
	private BorderPane      textContainer;
	private ScrollPane      scrollText;
	private CustomScrollBar customScrollBar;

	private Text       numberLineText;
	private TextFlow   numberLineFlow;
	private BorderPane numberContainer;
	private ScrollPane scrollNumberLine;

	private TextTools textTools;
	private History   history;
	private Painter   painter;

	private final Insets PADDING_NUMBER = new Insets(12, 5, 200, 10);
	private final Insets PADDING_TEXT   = new Insets(12, 15, 200, 15);

	private String tabulation;

	private static final Font FONT = new Font("Consolas", 14);

	public TextEditor() {

		textTools = new TextTools(this, false);
		history = new History(textTools);
		painter = new Painter(textTools);

		panelNumeration();
		panelEditor();
		panelRoot();

		tabulation = Tabulation.SHORT.toString();
		setTheme(Theme.DARK);
		setOnEvents();

		getChildren().addAll(root);
	}

	/**
	 * TODO describe this method
	 */
	private void panelNumeration() {

		activeLineNumber = textTools.getCaret().getActiveLineNumber();

		numberLineText = textTools.getNumerationText();
		numberLineText.setFont(FONT);

		numberLineFlow = new TextFlow();
		numberLineFlow.getChildren().add(numberLineText);
		numberLineFlow.setPadding(PADDING_NUMBER);
		numberLineFlow.setLineSpacing(1);
		numberLineFlow.setTextAlignment(TextAlignment.CENTER);

		numberContainer = new BorderPane();
		numberContainer.getChildren().add(activeLineNumber);
		numberContainer.setCenter(numberLineFlow);

		scrollNumberLine = new ScrollPane();
		scrollNumberLine.setContent(numberContainer);
		scrollNumberLine.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		scrollNumberLine.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		scrollNumberLine.setFitToWidth(true);
		scrollNumberLine.setFitToHeight(true);

		activeLineNumber.widthProperty().bind(scrollNumberLine.widthProperty());
	}

	/**
	 * TODO describe this method
	 */
	private void panelEditor() {

		activeLineText = textTools.getCaret().getActiveLineText();
		activeLineTextFit = textTools.getCaret().getActiveLineTextFit();

		textFlow = textTools.getTextFlow();

		textContainer = new BorderPane();
		textContainer.setPadding(PADDING_TEXT);
		textContainer.getChildren()
		             .addAll(activeLineText,
		                     activeLineTextFit,
		                     textTools.getSelection().getSelectionPath(),
		                     textTools.getCaret().getCaretPath());
		textContainer.setCenter(textFlow);
		textContainer.setCursor(Cursor.TEXT);

		scrollText = new ScrollPane();
		scrollText.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		scrollText.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		scrollText.setContent(textContainer);
		scrollText.setCursor(Cursor.TEXT);

		customScrollBar = new CustomScrollBar(scrollText, textContainer);

		textPane = new BorderPane();
		textPane.setCenter(scrollText);
		textPane.getChildren().add(customScrollBar.getvBar());
		textPane.getChildren().add(customScrollBar.gethBar());

		scrollText.vvalueProperty()
		          .bindBidirectional(scrollNumberLine.vvalueProperty());
		activeLineText.widthProperty().bind(textContainer.widthProperty());
		activeLineTextFit.widthProperty().bind(scrollText.widthProperty());
	}

	/**
	 * TODO describe this method
	 */
	private void panelStatus() {

	}

	/**
	 * TODO describe this method
	 */
	private void panelRoot() {

		root = new BorderPane();
		root.setCenter(textPane);
		root.setLeft(scrollNumberLine);
		root.setFocusTraversable(false);
		root.getStylesheets().add("style.css");
	}

	/**
	 * Paramètres les événements de l'ensemble de la scène.
	 */
	private void setOnEvents() {

		root.setOnKeyTyped(e -> {
			String keyChar = e.getCharacter();
			int    code    = keyChar.codePointAt(0);
			if (!e.isAltDown() &&
			    (e.isControlDown() || code == 8 || code == 9 || code == 27 ||
			     code == 127)) {
				return;
			}
			textTools.insertOnCaret(keyChar);
		});

		scrollText.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
			switchKeyCode(e);
			e.consume();
		});

		scrollText.focusedProperty().addListener((arg0, oldValue, newValue) -> {
			if (newValue) {
				textTools.getCaret().focusAcquired();
			} else {
				textTools.getCaret().focusLost();
			}
		});

		scrollText.setOnMousePressed(e -> {
			if (e.getX() < textContainer.getWidth() &&
			    e.getY() < textContainer.getHeight()) {
				return;
			}
			moveCaret(e.getX() + Math.max(0,
			                              textContainer.getWidth() -
			                              scrollText.getWidth()) *
			                     scrollText.getHvalue(),
			          e.getY() + Math.max(0,
			                              textContainer.getHeight() -
			                              scrollText.getHeight()) *
			                     scrollText.getVvalue());
		});
		textContainer.setOnMousePressed(e -> moveCaret(e.getX(), e.getY()));

		textContainer.setOnMouseDragged(e -> textTools.mouseDrag(
				e.getX() - PADDING_TEXT.getLeft(), e.getY()));

		textContainer.setOnMouseClicked(mouseEvent -> {
			if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
				int count = mouseEvent.getClickCount();
				if (count == 2) {
					textTools.selectCaretWord();
				} else if (count == 3) {
					textTools.selectCurrentLine();
				}
			}
		});
	}

	/**
	 * Détermine l'action appliquée relative à un ensemble de touches clavier.
	 *
	 * @param e l'événement clavier
	 */
	private void switchKeyCode(KeyEvent e) {

		boolean isCtrl  = e.isControlDown();
		boolean isShift = e.isShiftDown();
		boolean isAlt   = e.isAltDown();

		KeyCode code = e.getCode();

		if (!isCtrl && !isShift && !isAlt) {                    // Nothing

			switch (code) {
			case BACK_SPACE:
				textTools.relativeDeleteText(-1);
				break;
			case DELETE:
				textTools.relativeDeleteText(1);
				break;
			case TAB:
				textTools.tabulationKey();
				break;
			case LEFT:
				textTools.relativeMoveCaret(-1);
				break;
			case RIGHT:
				textTools.relativeMoveCaret(1);
				break;
			case UP:
				textTools.upKey();
				break;
			case DOWN:
				textTools.downKey();
				break;
			case ESCAPE:
				textTools.getSelection().stopSelection();
				break;
			case HOME:
				break;
			case END:
				break;
			}
		} else if (isCtrl && !isShift && !isAlt) {              // Only CTRL
			switch (code) {
			case A:
				textTools.selectAll();
				break;
			case C:
				textTools.ctrlCKey();
				break;
			case V:
				textTools.pasteClipboard();
				break;
			case X:
				textTools.ctrlXKey();
				break;
			case Z:
				history.undo();
				break;
			case L:
				textTools.selectCurrentLine();
				break;
			case RIGHT:
				textTools.moveCaretToNextWord();
				break;
			case LEFT:
				textTools.moveCaretToPreviousWord();
				break;
			}
		} else if (!isCtrl && !isShift) {                       // Only ALT
			switch (code) {
			case DOWN:
				break;
			case UP:
				break;
			}
		} else if (!isCtrl && !isAlt) {                         // Only SHIFT
			switch (code) {
			case DOWN:
				textTools.growSelection("DOWN");
				break;
			case UP:
				textTools.growSelection("UP");
				break;
			case LEFT:
				textTools.growSelection("LEFT");
				break;
			case RIGHT:
				textTools.growSelection("RIGHT");
				break;
			}
		} else if (isCtrl && isShift && !isAlt) {               // CTRL + SHIFT
			switch (code) {
			case RIGHT:
				textTools.growSelection("RIGHT_WORD");
				break;
			case LEFT:
				textTools.growSelection("LEFT_WORD");
				break;
			case ENTER:
				break;
			case K:

				break;
			case Z:
				history.redo();
				break;
			}
		} else if (!isCtrl) {                                   // SHIFT + ALT
			switch (code) {
			case Z:
				history.clear();
				break;
			case UP:
				break;
			case DOWN:
				break;
			}
		}
	}

	/**
	 * Déplace le curseur selon les positions indiquées aux caractères
	 * correspondants.
	 *
	 * @param x la position horizontale
	 * @param y la position verticale
	 */
	private void moveCaret(double x, double y) {

		textTools.moveCaret(x - PADDING_TEXT.getLeft(), y - 3);
	}

	/**
	 * TODO describe this method
	 */
	private void smoothScroll(boolean isV, double value) {

		Transition scroll = new Transition() {
			{
				setCycleDuration(Duration.millis(300));
				setInterpolator(Interpolator.EASE_OUT);
			}

			@Override
			protected void interpolate(double frac) {

				if (isV) {
					double origin = scrollText.getVvalue();
					scrollText.setVvalue(origin + (value - origin) * frac);
				} else {
					double origin = scrollText.getHvalue();
					scrollText.setHvalue(origin + (value - origin) * frac);
				}
			}
		};

		scroll.playFromStart();
	}

	/**
	 * TODO describe this method
	 */
	public void autoScroll() {

		Caret caret = textTools.getCaret();

		double xCaret =
				caret.getCaretPath().layoutBoundsProperty().get().getMaxX() +
				PADDING_TEXT.getLeft();
		double yMaxCaret =
				caret.getCaretPath().layoutBoundsProperty().get().getMaxY() +
				PADDING_TEXT.getTop();
		double yMinCaret =
				caret.getCaretPath().layoutBoundsProperty().get().getMinY() -
				PADDING_TEXT.getTop();

		double scrollTextWidth     = scrollText.getWidth();
		double textContainerWidth  = textContainer.getWidth();
		double scrollTextHeight    = scrollText.getHeight();
		double textContainerHeight = textContainer.getHeight();

		double xMaxViewport = (textContainerWidth - scrollTextWidth) *
		                      scrollText.getHvalue() + scrollTextWidth;
		double xMinViewport = xMaxViewport - scrollTextWidth;
		double yMaxViewport = (textContainerHeight - scrollTextHeight) *
		                      scrollText.getVvalue() + scrollTextHeight;
		double yMinViewport = yMaxViewport - scrollTextHeight;

		if (yMaxCaret + 3 > yMaxViewport) {
			smoothScroll(true,
			             (yMaxCaret + caret.getCaretPath()
			                               .layoutBoundsProperty()
			                               .get()
			                               .getHeight() - scrollTextHeight) /
			             (textContainerHeight - scrollTextHeight));
		} else if (yMinCaret + 3 < yMinViewport) {
			smoothScroll(true,
			             yMinCaret / (textContainerHeight - scrollTextHeight));
		}

		if (xCaret > xMaxViewport) {

			smoothScroll(false,
			             (xCaret - scrollTextWidth + PADDING_TEXT.getRight()) /
			             (textContainerWidth - scrollTextWidth));
		} else if (xCaret < xMinViewport) {
			smoothScroll(false,
			             (xCaret - PADDING_TEXT.getLeft()) /
			             (textContainerWidth - scrollTextWidth));
		}
	}

	/**
	 * Insert une chaîne de caractères après la position courante du curseur.
	 *
	 * @param text la chaîne de caractères
	 */
	public void insertOnCaret(String text) { textTools.insertOnCaret(text); }

	/**
	 * Rajoute une chaîne de caractères à la suite de l'ensemble du texte.
	 *
	 * @param text la chaîne de caractères
	 */
	public void append(String text) {textTools.append(text); }

	/**
	 * Efface l'ensemble du texte
	 */
	public void clear() {textTools.clear(); }

	/**
	 * TODO describe this method
	 */
	public void addRegexColor(String regex, Color color) {

		painter.addRegexColor(new RegexColor(regex, color));
	}

	public enum Tabulation {
		SHORT(" ".repeat(4)), LONG(" ".repeat(8)), SYSTEM("\t");

		private String type;

		Tabulation(String type) { this.type = type; }

		@Override
		public String toString() { return type; }
	}

	/** @return the painter */
	public Painter getPainter() { return painter; }

	/**
	 * Obtention de l'ensemble du texte.
	 *
	 * @return Le texte correspondant
	 */
	public String getText() { return textTools.getText(); }

	/** @return the history */
	public History getHistory() { return history; }

	/** @return the tabulation */
	public String getTabulation() { return tabulation; }

	/**
	 * Paramétrage du type de tabulation.
	 *
	 * @param type trois choix possibles : 4,8 espaces ou le caractère \t
	 */
	public void setTabulation(Tabulation type) {

		tabulation = type.toString();
	}

	/** @return the FONT */
	public static Font getFONT() { return FONT; }

	/**
	 * TODO describe this method
	 */
	public void setReadOnlyMode(boolean isReadOnly) {

		textTools.setReadOnly(isReadOnly);
	}

	/**
	 * L'application du thème met à jour dynamiquement l'ensemble des
	 * composants.
	 *
	 * @param themeToApply Le thème à appliqué.
	 */
	public void setTheme(Theme themeToApply) {

		textTools.getSelection().setColor(themeToApply.colorSelection);

		textTools.setColorText(themeToApply.colorText);
		painter.setDefaultColor(themeToApply.colorText);

		numberLineText.setFill(themeToApply.colorNumber);
		numberLineText.setSelectionFill(themeToApply.colorCaret);

		Background backgroundOfEditor = new Background(new BackgroundFill(
				themeToApply.colorBackgroundEditor,
				CornerRadii.EMPTY,
				Insets.EMPTY));
		textContainer.setBackground(backgroundOfEditor);
		numberContainer.setBackground(backgroundOfEditor);
		scrollText.setStyle("-fx-background: #" +
		                    themeToApply.colorBackgroundEditor.toString()
		                                                      .substring(2));

		Caret caret = textTools.getCaret();
		caret.setColorCaret(themeToApply.colorCaret);
		caret.setColorActive(themeToApply.colorLineActive);

		customScrollBar.customize(themeToApply.colorBar,
		                          themeToApply.barThickness,
		                          themeToApply.barArcCorner);
	}
}