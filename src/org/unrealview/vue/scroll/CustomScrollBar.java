/*
 * CustomScrollBar.java               date: 06/02/2020 17:57
 * IUT info1 2019/2020 groupe 3, pas de droits d'auteur
 */

package org.unrealview.vue.scroll;

import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class CustomScrollBar {

	private final static int PADDING = 2;

	private int thickness;

	private ScrollPane scrollPane;
	private Node       internal;

	private Rectangle vBar;
	private Rectangle hBar;

	public CustomScrollBar(ScrollPane scrollPane, Node internal) {

		// FIXME chevauchement des deux bars en position maximum

		this.scrollPane = scrollPane;
		this.internal = internal;
		thickness = 8;

		setupHBar();
		setupVBar();

		scrollPane.vvalueProperty()
		          .addListener((observable, oldValue, newValue) -> computeYBar());
		scrollPane.hvalueProperty()
		          .addListener((observable, oldValue, newValue) -> computeXBar());

		ChangeListener<Bounds> boundsChangeListener =
				(observable, oldValue, newValue) -> {
					computeHeightBar();
					computeWidthBar();
					computeYBar();
					computeXBar();
				};
		scrollPane.layoutBoundsProperty().addListener(boundsChangeListener);
		internal.layoutBoundsProperty().addListener(boundsChangeListener);
	}

	/**
	 * TODO describe this method
	 */
	private void setupHBar() {

		hBar = new Rectangle(0, PADDING, 0, thickness);
		hBar.setOpacity(0.5);
		hBar.yProperty()
		    .bind(scrollPane.heightProperty().add(-thickness - PADDING));

		hBar.setOnMouseEntered(e -> setOpacity(hBar, 1));
		hBar.setOnMouseExited(e -> setOpacity(hBar, 0.5));

		var delta = new Object() {
			double x = 0;
		};

		hBar.setOnMousePressed(e -> delta.x = e.getX());
		hBar.setOnMouseDragged(e -> {
			setOpacity(hBar, 1);
			scrollPane.setHvalue(scrollPane.getHvalue() + (e.getX() - delta.x) /
			                                              (scrollPane.getLayoutBounds()
			                                                         .getWidth() -
			                                               hBar.getWidth()));
			delta.x = e.getX();
		});
		hBar.setOnMouseReleased(e -> setOpacity(hBar, 0.5));
	}

	/**
	 * TODO describe this method
	 */
	private void setupVBar() {

		vBar = new Rectangle(0, PADDING, thickness, 0);
		vBar.setOpacity(0.5);
		vBar.xProperty()
		    .bind(scrollPane.widthProperty().add(-thickness - PADDING));

		vBar.setOnMouseEntered(e -> setOpacity(vBar, 1));
		vBar.setOnMouseExited(e -> setOpacity(vBar, 0.5));

		var delta = new Object() {
			double y = 0;
		};

		vBar.setOnMousePressed(e -> delta.y = e.getY());
		vBar.setOnMouseDragged(e -> {
			setOpacity(vBar, 1);
			scrollPane.setVvalue(scrollPane.getVvalue() + (e.getY() - delta.y) /
			                                              (scrollPane.getLayoutBounds()
			                                                         .getHeight() -
			                                               vBar.getHeight()));
			delta.y = e.getY();
		});
		vBar.setOnMouseReleased(e -> setOpacity(vBar, 0.5));
	}

	/**
	 * TODO describe this method
	 */
	private void computeHeightBar() {

		double heightInternal = internal.getLayoutBounds().getHeight();
		double heightScroll   = scrollPane.getLayoutBounds().getHeight();

		if (heightInternal <= heightScroll) {
			setOpacity(vBar, 0);
			vBar.setHeight(0);
		} else {
			setOpacity(vBar, 0.5);
			vBar.setHeight((Math.sqrt(heightScroll) /
			                Math.sqrt(heightInternal) * heightScroll));
		}
	}

	/**
	 * TODO describe this method
	 */
	private void computeYBar() {

		vBar.setY(
				(scrollPane.getLayoutBounds().getHeight() - vBar.getHeight()) *
				scrollPane.getVvalue());
	}

	/**
	 * TODO describe this method
	 */
	private void computeWidthBar() {

		double widthInternal = internal.getLayoutBounds().getWidth();
		double widthScroll   = scrollPane.getLayoutBounds().getWidth();

		if (widthInternal <= widthScroll) {
			setOpacity(hBar, 0);
			hBar.setWidth(0);
		} else {
			setOpacity(hBar, 0.5);
			hBar.setWidth((Math.sqrt(widthScroll) / Math.sqrt(widthInternal) *
			               widthScroll));
		}
	}

	/**
	 * TODO describe this method
	 */
	private void computeXBar() {

		hBar.setX((scrollPane.getLayoutBounds().getWidth() - hBar.getWidth()) *
		          scrollPane.getHvalue());
	}

	/**
	 * TODO describe this method
	 */
	private void setOpacity(Rectangle bar, double opacity) {

		Transition fade = new Transition() {
			{
				setCycleDuration(Duration.millis(300));
				setInterpolator(Interpolator.EASE_OUT);
			}

			@Override
			protected void interpolate(double frac) {

				double origin = bar.getOpacity();
				bar.setOpacity(origin + (opacity - origin) * frac);
			}
		};

		fade.playFromStart();
	}

	/**
	 * TODO describe this method
	 */
	public void customize(Color colorBar, double thickness, double arcCorner) {

		hBar.setFill(colorBar);
		vBar.setFill(colorBar);
		hBar.setHeight(thickness);
		vBar.setWidth(thickness);
		vBar.setArcHeight(arcCorner);
		vBar.setArcWidth(arcCorner);
		hBar.setArcHeight(arcCorner);
		hBar.setArcWidth(arcCorner);
	}

	/** @return the vBar */
	public Rectangle getvBar() { return vBar; }

	/** @return the hBar */
	public Rectangle gethBar() { return hBar; }
}