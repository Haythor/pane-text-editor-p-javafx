/*
 * Theme.java                    date: 29/01/2020 15:03
 * IUT info1 2019/2020 groupe 3, pas de droits d'auteur
 */

package org.unrealview.vue;

import javafx.scene.paint.Color;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class Theme {

	/** Couleur d'arrière-plan de l'éditeur */
	Color colorBackgroundEditor;

	/** Couleur du texte par défaut */
	Color colorText;

	/** Couleur du texte de la numérotation par défaut */
	Color colorNumber;

	/** Couleur du rectangle symbolisant la ligne active */
	Color colorLineActive;

	/** Couleur du curseur (utilisé pour Le nombre de numérotation active) */
	Color colorCaret;

	/** Couleur de la sélection */
	Color colorSelection;

	/** TODO describe this attribute */
	Color colorBar;

	/** TODO describe this attribute */
	double barThickness;

	/** TODO describe this attribute */
	double barArcCorner;

	/** Thème sombre */
	public final static Theme DARK  = new Theme("1A1A1A",
	                                            "E6E6E6",
	                                            "595959",
	                                            "262626",
	                                            "5A98D6",
	                                            "41444D",
	                                            "DE7F26",
	                                            8,
	                                            10);
	/** Thème clair */
	public final static Theme LIGHT = new Theme("F2F2F2",
	                                            "1A1A1A",
	                                            "999999",
	                                            "E0E0E0",
	                                            "E64E97",
	                                            "C7C8D9",
	                                            "DE7F26",
	                                            8,
	                                            10);


	public Theme(String colorBackgroundEditor, String colorText,
	             String colorNumber, String colorLineActive, String colorCaret,
	             String colorSelection, String colorBar, double barThickness,
	             double barArcCorner) {

		this.colorBackgroundEditor = Color.valueOf(colorBackgroundEditor);
		this.colorText = Color.valueOf(colorText);
		this.colorNumber = Color.valueOf(colorNumber);
		this.colorLineActive = Color.valueOf(colorLineActive);
		this.colorCaret = Color.valueOf(colorCaret);
		this.colorSelection = Color.valueOf(colorSelection);
		this.colorBar = Color.valueOf(colorBar);
		this.barThickness = barThickness;
		this.barArcCorner = barArcCorner;
	}
}