/*
 * Selection.java                date: 29/01/2020 08:15
 * IUT info1 2019/2020 groupe 3, pas de droits d'auteur
 */

package org.unrealview.model;

import javafx.scene.paint.Color;
import javafx.scene.shape.Path;
import javafx.scene.shape.PathElement;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.TextFlow;
import org.unrealview.model.text.TextTools;

import java.util.Arrays;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class Selection {

	private TextTools textTools;
	private TextFlow  textFlow;

	private Path selectionPath;

	private boolean isSelected;

	private int anchorPoint;
	private int movablePoint;

	public Selection(TextTools textTools) {

		this.textTools = textTools;
		textFlow = textTools.getTextFlow();

		selectionPath = new Path();
		selectionPath.setSmooth(false);
		selectionPath.setStrokeWidth(1);
		selectionPath.setStrokeType(StrokeType.CENTERED);
		selectionPath.translateXProperty().bind(textFlow.layoutXProperty());
		selectionPath.translateYProperty()
		             .bind(textFlow.layoutYProperty().add(-3));

		stopSelection();
	}

	/**
	 * Arrête la sélection en cours.
	 * Efface les éléments graphiques correspondant, et attribuer une valeur
	 * interdite aux points.
	 */
	public void stopSelection() {

		isSelected = false;
		selectionPath.getElements().clear();
		anchorPoint = movablePoint = -1;
	}

	/**
	 * Démarre une sélection.
	 * Le point spécifié, détermine le point d'ancrage : celui-ci ne bougera
	 * pas lors de modification de la sélection.
	 * <p>
	 * Bien que démarrée et considérée comme active, aucune forme n'est
	 * actuellement visible.
	 *
	 * @param anchorPoint l'index du point d'ancrage.
	 */
	public void startSelection(int anchorPoint) {

		this.anchorPoint = anchorPoint;
		this.movablePoint = anchorPoint;
		isSelected = true;
	}

	/**
	 * Met à jour le point mobile et la forme graphique de la sélection.
	 *
	 * @param movablePoint L'index du point mobile
	 */
	public void updateSelection(int movablePoint) {

		if (!isSelected) {
			return;
		}

		this.movablePoint = movablePoint;
		PathElement[] caretPathElement =
				textFlow.rangeShape(getStartIndex(), getEndIndex());

		selectionPath.getElements().clear();
		Arrays.stream(caretPathElement)
		      .forEachOrdered(pathElement -> selectionPath.getElements()
		                                                  .add(pathElement));
	}

	/**
	 * Calcul de l'index minimum de la sélection en cours (même si aucune
	 * sélection n'est en cours le point est quand même renvoyée).
	 *
	 * @return l'index de départ
	 */
	public int getStartIndex() { return Math.min(anchorPoint, movablePoint); }

	/**
	 * Calcul de l'index maximum de la sélection en cours (même si aucune
	 * sélection n'est en cours le point est quand même renvoyée).
	 *
	 * @return l'index de fin
	 */
	public int getEndIndex() { return Math.max(anchorPoint, movablePoint); }

	/** @return the selectionPath */
	public Path getSelectionPath() { return selectionPath; }

	/** @return the isSelected */
	public boolean isSelected() { return isSelected; }

	/** @return the anchorPoint */
	public int getAnchorPoint() { return anchorPoint; }

	/**
	 * Spécifie la nouvelle couleur de la sélection ; mis à jour dynamiquement.
	 *
	 * @param colorSelection la nouvelle couleur
	 */
	public void setColor(Color colorSelection) {

		selectionPath.setFill(colorSelection);
		selectionPath.setStroke(colorSelection);
	}
}