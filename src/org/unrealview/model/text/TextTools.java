/*
 * TextTools.java              date: 29/01/2020 09:07
 * IUT info1 2019/2020 groupe 3, pas de droits d'auteur
 */

package org.unrealview.model.text;

import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import org.unrealview.model.Caret;
import org.unrealview.model.Selection;
import org.unrealview.model.history.AddedText;
import org.unrealview.model.history.DeletedText;
import org.unrealview.vue.TextEditor;

import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class TextTools {

	private TextEditor textEditor;
	private Indexer    indexer;
	private TextFlow   textFlow;

	private ObservableList<Node> textList;

	private Text      numerationText;
	private Caret     caret;
	private Selection selection;

	private boolean isReadOnly;

	private final Service<Void> makeNumeration = new Service<>() {

		@Override
		protected Task<Void> createTask() {

			return new Task<>() {

				@Override
				protected Void call() {

					makeNumeration();
					return null;
				}
			};
		}
	};

	private final Service<Void> numberHighlightRefresh = new Service<>() {

		@Override
		protected Task<Void> createTask() {

			return new Task<>() {

				@Override
				protected Void call() {

					numberHighlightRefresh();
					return null;
				}
			};
		}
	};

	/**
	 * Chaîne de caractères correspondant au séparateur de lignes utiliser
	 * au sein de l'application.
	 */
	final static String LINE_SEPARATOR = "\n";

	public TextTools(TextEditor textEditor, boolean isReadOnly) {

		this.textEditor = textEditor;
		this.isReadOnly = isReadOnly;

		Text text = new Text("");
		text.setFont(TextEditor.getFONT());

		textFlow = new TextFlow();
		textFlow.setLineSpacing(1);
		textFlow.getChildren().add(text);

		textList = textFlow.getChildren();
		indexer = new Indexer(textList);

		numerationText = new Text("1");

		caret = new Caret(this);

		selection = new Selection(this);

		indexer.getIndexesLine().setOnSucceeded(e -> {
			makeNumeration.restart();
			makeNumeration.setOnSucceeded(r -> numberHighlightRefresh.restart());
		});

		indexer.getRefreshCurrentLine().setOnSucceeded(e -> {
			if (!makeNumeration.isRunning()) {
				numberHighlightRefresh.restart();
			}
		});
	}

	/**
	 * Recherche texte contenu entre deux index.
	 *
	 * @param startIndex l'index du caractère de départ
	 * @param endIndex   l'index du caractère final
	 * @return la chaîne de caractères correspondante
	 */
	private String textBetween(int startIndex, int endIndex) {

		int numberChar = indexer.getNumberChar();
		startIndex = Math.max(0, Math.min(startIndex, numberChar - 1));
		endIndex = Math.max(0, Math.min(endIndex, numberChar));

		int[] startIndexOfText = indexer.localIndex(startIndex);
		int[] endIndexOfText   = indexer.localIndex(endIndex);

		StringBuilder sb = new StringBuilder();
		// System.out.println("numberChar = " + numberChar);
		// System.out.println("endIndex = " + endIndex);

		if (endIndexOfText[0] == startIndexOfText[0]) { // Un text concerné

			sb.append(((Text) textList.get(startIndexOfText[0])).getText(),
			          startIndex - startIndexOfText[1],
			          endIndex - startIndexOfText[1]);
		} else if (endIndexOfText[0] - startIndexOfText[0] >= 1) {

			sb.append(((Text) textList.get(startIndexOfText[0])).getText()
			                                                    .substring(
					                                                    startIndex -
					                                                    startIndexOfText[1]));
			// System.out.println(
			// 		"startIndexOfText[0] + 1 = " + (startIndexOfText[0] + 1));
			// System.out.println(
			// 		"endIndexOfText[0] - 1 = " + (endIndexOfText[0] - 1));

			IntStream.rangeClosed(startIndexOfText[0] + 1,
			                      endIndexOfText[0] - 1)
			         .mapToObj(i -> ((Text) textList.get(i)))
			         .forEachOrdered(text -> sb.append(text.getText()));

			if (endIndex == numberChar) {
				sb.append(((Text) textList.get(endIndexOfText[0])).getText());
			} else {
				sb.append(((Text) textList.get(endIndexOfText[0])).getText(),
				          0,
				          endIndex - endIndexOfText[1]);
			}
		}

		return sb.toString();
	}

	/**
	 * Standardise les séquences de fin de ligne selon celle utilisée par
	 * l'application.
	 *
	 * @param stringToCheck la chaîne a vérifié
	 * @return la chaîne standardisé
	 */
	private String normalizeEndLine(String stringToCheck) {

		if (stringToCheck.contains("\n") && stringToCheck.contains("\r")) {
			stringToCheck = stringToCheck.replaceAll("\r\n", LINE_SEPARATOR);
		} else if (stringToCheck.contains("\r") &&
		           !stringToCheck.contains("\n")) {
			stringToCheck = stringToCheck.replaceAll("\r", LINE_SEPARATOR);
		}

		return stringToCheck;
	}

	/**
	 * Standardise les séquences de fin de ligne selon celle utilisée par le
	 * système.
	 *
	 * @param stringToCheck la chaîne a vérifié
	 * @return la chaîne standardisé
	 */
	private String setEndLineToSystem(String stringToCheck) {

		String systemLineSeparator = System.lineSeparator();

		if (stringToCheck.contains(LINE_SEPARATOR) &&
		    !systemLineSeparator.equals(LINE_SEPARATOR)) {
			stringToCheck = stringToCheck.replaceAll(LINE_SEPARATOR,
			                                         systemLineSeparator);
		}

		return stringToCheck;
	}

	/**
	 * Génère le texte correspondant au numéro de ligne. Si la numérotation
	 * actuelle comporte trop de numéros alors les derniers sont enlevés
	 * sinon on n'en rajoute pour atteindre le nombre de lignes correspondant
	 * au texte.
	 */
	private void makeNumeration() {

		int numberLine = indexer.getNumberLine();

		int index;
		int countLineOfNumeration =
				(int) numerationText.getText().lines().count();
		StringBuilder sb = new StringBuilder(numerationText.getText());

		if (countLineOfNumeration > numberLine) {

			index = sb.length();

			int tooLineCount = countLineOfNumeration - numberLine;

			while (tooLineCount != 0) {
				index = sb.lastIndexOf(LINE_SEPARATOR, --index);
				tooLineCount--;
			}

			numerationText.setText(sb.substring(0, index));
		} else if (countLineOfNumeration < numberLine) {

			IntStream.range(countLineOfNumeration, numberLine)
			         .forEachOrdered(i -> sb.append(LINE_SEPARATOR)
			                                .append(i + 1));
			numerationText.setText(sb.toString());
		}
	}

	/**
	 * Actualisation du nombre correspondant à la ligne active.
	 */
	private void numberHighlightRefresh() {

		int numberLine = indexer.getCurrentLine();
		int index =
				numerationText.getText().indexOf(String.valueOf(numberLine));
		numerationText.setSelectionStart(index);
		numerationText.setSelectionEnd(
				index + String.valueOf(numberLine).length());
	}

	/**
	 * Vérifier la présence d'objets Text vide et les enlèvent s'il y en a.
	 */
	private void checkEmptyText() {

		int size = textList.size() - 1;

		// On évite de supprimer le dernier Text si il est vide
		if (size == 1) {
			return;
		}

		IntStream.range(0, size)
		         .mapToObj(i -> textList.get(i))
		         .filter(text -> ((Text) text).getText().isEmpty())
		         .forEachOrdered(text -> textList.remove(text));
	}

	/**
	 * Détermine le nombre de caractères à parcourir à partir de l'index
	 * d'origine pour atteindre le mot suivant.
	 *
	 * @param originIndex le point d'origine de la recherche
	 * @return l'offset correspondant
	 */
	private int offsetNextWord(int originIndex) {

		Scanner scanner = new Scanner(textBetween(originIndex,
		                                          indexer.endOfLine(indexer.getCurrentLine())));
		return scanner.hasNext() ? scanner.next().length() + 1 : 1;
	}

	/**
	 * Détermine le nombre de caractères à parcourir à partir de l'index
	 * d'origine pour atteindre le mot précédent.
	 *
	 * @param originIndex le point d'origine de la recherche
	 * @return l'offset correspondant
	 */
	private int offsetPreviousWord(int originIndex) {

		StringBuilder sb =
				new StringBuilder(textBetween(indexer.startOfLine(indexer.getCurrentLine()),
				                              originIndex));
		Scanner scanner = new Scanner(sb.reverse().toString());

		return scanner.hasNext() ? -(scanner.next().length() + 1) : -1;
	}

	/**
	 * TODO describe this method
	 */
	private int insertTabulation(int index) {

		String tabulation = textEditor.getTabulation();

		if (tabulation.equals("\t")) {
			insertOn(index, "\t");
			return 1;
		} else {
			int indexLine       =
					indexer.startOfLine(indexer.indexToLine(index));
			int length          = tabulation.length();
			int effectiveLength = length - (index - indexLine - 1) % length;
			insertOn(index, tabulation.substring(0, effectiveLength));
			return effectiveLength;
		}
	}

	/**
	 * Ajoute le texte à la position donnée. Si une sélection est en cours,
	 * celle-ci est stoppée et son contenu est supprimé.
	 * <p>
	 * Le texte ajouté et normalisé selon les fins de ligne locale.
	 * <p>
	 * Est additionné le nombre de caractères à ajouter à la variable de
	 * comptage de nombre de caractères global.
	 * <p>
	 * L'indexation des fins de ligne et des fins de texte et faite.
	 * <p>
	 * La numérotation est mise à jour si le texte contient des fins de ligne.
	 * <p>
	 * La gestion de l'historique et prise en compte.
	 * <p>
	 * Un appel au Painter est fait.
	 *
	 * @param indexChar index d'insertion par rapport à l'ensemble du texte
	 *                  (Si l'index n'est pas compris entre l'intervalle
	 *                  zéro et le nombre de caractères celui-ci sera modifié)
	 * @param input     le texte qui doit être ajouté
	 */
	public void insertOn(int indexChar, String input) {

		if (isReadOnly) {
			return;
		}

		if (selection.isSelected()) {
			indexChar = selection.getStartIndex();
			deleteBetween(indexChar, selection.getEndIndex());
			selection.stopSelection();
		}

		input = normalizeEndLine(input);
		int inputLength = input.length();
		if (inputLength == 0) {
			return;
		}
		int numberChar = indexer.getNumberChar();
		int[] indexOfTextToEdit = indexer.localIndex(Math.max(0,
		                                                      Math.min(indexChar,
		                                                               numberChar -
		                                                               1)));
		indexChar = Math.max(0, Math.min(indexChar, numberChar));
		indexer.addToNumberChar(inputLength);
		Text textToEdit = (Text) textList.get(indexOfTextToEdit[0]);

		StringBuilder sb = new StringBuilder(textToEdit.getText());
		sb.insert(indexChar - indexOfTextToEdit[1], input);
		textToEdit.setText(sb.toString());

		indexer.getIndexesLine().restart();
		indexer.getIndexesText().restart();
		textEditor.getHistory()
		          .addElement(new AddedText(indexChar,
		                                    indexChar + inputLength));
		caret.setIndexCaret(indexChar + inputLength, true);
		textEditor.getPainter().paint();
	}

	/**
	 * Supprime dans la mesure du possible le texte compris entre l'index de
	 * départ et l'index final.
	 * <p>
	 * La variable de comptage du nombre de caractères est soustraite du
	 * nombre de caractères supprimés.
	 * <p>
	 * Une vérification de d'objets de Text vide est faite.
	 * <p>
	 * L'indexation des fins de lignée des fins de texte est effectuée.
	 * <p>
	 * Si le texte supprimé contenait des fins de ligne la numérotation est
	 * mise à jour.
	 * <p>
	 * La gestion de l'historique et prise en compte.
	 * <p>
	 * Un appel au Painter est fait.
	 *
	 * @param startIndex l'index de départ par rapport à l'ensemble du texte
	 * @param endIndex   L'index final par rapport à l'ensemble du texte
	 */
	public void deleteBetween(int startIndex, int endIndex) {

		// FIXME selection sûr du vide après suppression

		/* Forcément rien à supprimer */
		if (startIndex >= endIndex || isReadOnly) {
			return;
		}

		String deletedText       = textBetween(startIndex, endIndex);
		int    lengthDeletedText = deletedText.length();

		if (lengthDeletedText == 0) { // Pas de texte trouver
			return;
		}

		int numberChar = indexer.getNumberChar();
		startIndex = Math.max(0, Math.min(startIndex, numberChar - 1));
		endIndex = Math.max(0, endIndex);

		int[] startIndexOfText = indexer.localIndex(startIndex);
		int[] endIndexOfText   = indexer.localIndex(endIndex);
		indexer.addToNumberChar(-lengthDeletedText);

		Text          currentText   = (Text) textList.get(startIndexOfText[0]);
		StringBuilder stringBuilder = new StringBuilder(currentText.getText());

		int bound = endIndexOfText[0];
		for (int i = startIndexOfText[0] + 1; i <= bound; bound--) {
			Text text = (Text) textList.get(i);
			stringBuilder.append(text.getText());
			textList.remove(text);
		}

		stringBuilder.replace(startIndex - startIndexOfText[1],
		                      endIndex - startIndexOfText[1],
		                      "");

		currentText.setText(stringBuilder.toString());

		checkEmptyText();

		indexer.getIndexesLine().restart();
		indexer.getIndexesText().restart();
		textEditor.getHistory()
		          .addElement(new DeletedText(deletedText, startIndex));
		caret.setIndexCaret(startIndex, false);
		textEditor.getPainter().paint();
	}

	/**
	 * Déplace le curseur de l'offset indiqué par rapport à sa position
	 * actuelle.
	 *
	 * @param offset nombre de caractères de décalage
	 */
	public void relativeMoveCaret(int offset) {

		caret.relativeMoveCaret(offset);
		selection.stopSelection();
	}

	/**
	 * Déplace le curseur au caractère donné par les coordonnées.
	 *
	 * @param x coordonnée horizontale
	 * @param y coordonnée verticale
	 */
	public void moveCaret(double x, double y) {

		caret.setIndexCaret(textFlow.hitTest(new Point2D(x, y))
		                            .getInsertionIndex(), false);
		selection.stopSelection();
	}

	/**
	 * Déplace le curseur au caractère donné par les coordonnées. Démarre une
	 * sélection si elle n'est pas en cours et actualise le point mobile dans
	 * le cas contraire.
	 *
	 * @param x coordonnée horizontale
	 * @param y coordonnée verticale
	 */
	public void mouseDrag(double x, double y) {

		int charIndex =
				textFlow.hitTest(new Point2D(x, y)).getInsertionIndex();
		caret.setIndexCaret(charIndex, false);

		if (selection.isSelected()) {
			selection.updateSelection(charIndex);
		} else {
			selection.startSelection(charIndex);
		}
	}

	/**
	 * Ajoute après le curseur la chaîne de caractères donnés en paramètre.
	 *
	 * @param input la chaîne de caractères à ajouter
	 */
	public void insertOnCaret(String input) {

		insertOn(caret.getCurrentIndex(), input);
	}

	/**
	 * Si une sélection est en cours, le texte correspondant sera supprimé et
	 * le curseur sera placé au début de celle-ci.
	 * Sinon sous réserve qu'il en est un le caractère précédent le curseur
	 * sera supprimé.
	 */
	public void relativeDeleteText(int offset) {

		if (selection.isSelected()) {
			insertOn(selection.getStartIndex(), "");
			return;
		}

		int indexCaret = caret.getCurrentIndex();
		if (offset > 0) {
			deleteBetween(indexCaret, indexCaret + offset);
		} else {
			deleteBetween(indexCaret + offset, indexCaret);
		}
	}

	/**
	 * Effectue l'action de la touche flèche du haut.
	 * Positionne le curseur sur la ligne juste au-dessus sous réserve
	 * qu'elle existe.
	 */
	public void upKey() {

		int currentLine = indexer.getCurrentLine();
		selection.stopSelection();
		// TODO update currentLine
		int indexCaret        = caret.getCurrentIndex();
		int indexPreviousLine = indexer.startOfLine(currentLine - 1);
		int indexLine         = indexer.startOfLine(currentLine);
		caret.setIndexCaret(indexPreviousLine +
		                    Math.min(indexLine - indexPreviousLine,
		                             indexCaret - indexLine), false);
	}

	/**
	 * Effectue l'action de la touche flèche du haut.
	 * Positionne le curseur sur la ligne juste en dessous sous réserve
	 * qu'elle existe.
	 */
	public void downKey() {

		int currentLine = indexer.getCurrentLine();
		selection.stopSelection();
		int indexCaret    = caret.getCurrentIndex();
		int indexLine     = indexer.startOfLine(currentLine);
		int endLine       = indexer.endOfLine(currentLine);
		int indexNextLine = indexer.endOfLine(currentLine + 1);
		caret.setIndexCaret(endLine + Math.min(indexCaret - indexLine,
		                                       indexNextLine - endLine),
		                    false);
	}

	/**
	 * Effectue l'action de la touche tabulation.
	 * Insert la suite de caractères définis à la position du curseur.
	 * S'il s'agit d'une suite d'espaces, l'effet d'alignement sera imité.
	 */
	public void tabulationKey() {

		// FIXME plante lors de l'appuie continue

		if (selection.isSelected()) {
			int startSelection  = selection.getStartIndex();
			int endSelection    = selection.getEndIndex();
			int start           = indexer.indexToLine(startSelection);
			int end             = indexer.indexToLine(endSelection);
			int effectiveLength = 0, anchorPoint = 0;

			selection.stopSelection();

			for (int i = start; i <= end; ++i) {
				effectiveLength += insertTabulation(indexer.startOfLine(i) + 1);

				if (i == start) {
					anchorPoint = startSelection + effectiveLength;
				}

				indexer.getIndexesLine().cancel();
				indexer.refreshIndexLine();
			}
			selection.startSelection(anchorPoint);
			selection.updateSelection(endSelection + effectiveLength);
			caret.setIndexCaret(selection.getEndIndex(), false);
		} else {
			insertTabulation(caret.getCurrentIndex());
		}
	}

	/**
	 * Effectue une sélection sur l'ensemble du texte présent.
	 */
	public void selectAll() {

		int numberChar = indexer.getNumberChar();
		selection.startSelection(0);
		selection.updateSelection(numberChar);
		caret.setIndexCaret(numberChar, false);
	}

	/**
	 * Sélectionne la ligne sur laquelle le curseur est positionné. Sauf s'il
	 * s'agit de la première ligne, c'est le caractère de fin de ligne qui est
	 * sélectionnée en premier.
	 * <p>
	 * ex : <du texte "\nla ligne courante"\n une autre ligne>
	 */
	public void selectCurrentLine() {

		int currentLine    = indexer.getCurrentLine();
		int indexStartLine = indexer.startOfLine(currentLine);
		selection.startSelection(indexStartLine);
		selection.updateSelection(indexer.endOfLine(currentLine));
		caret.setIndexCaret(indexStartLine, false);
	}

	/**
	 * Effectuer une sélection sur le mot sur lequel le curseur se situe.
	 */
	public void selectCaretWord() {

		// FIXME manque un caractère au premier mot
		moveCaretToPreviousWord();
		int indexStartWord = caret.getCurrentIndex();
		moveCaretToNextWord();
		selection.startSelection(indexStartWord + 1);
		selection.updateSelection(caret.getCurrentIndex());
	}

	/**
	 * Effectue des modifications du point mobile de la sélection selon
	 * des mouvement correspondant à des actions de certaines touches de
	 * déplacement.
	 *
	 * @param type mot correspondant au déplacement ou à une touche
	 */
	public void growSelection(String type) {

		int anchorPoint;

		if (!selection.isSelected()) {
			anchorPoint = caret.getCurrentIndex();
		} else {
			anchorPoint = selection.getAnchorPoint();
		}
		switch (type) {
		case "UP":
			upKey();
			break;
		case "DOWN":
			downKey();
			break;
		case "LEFT":
			relativeMoveCaret(-1);
			break;
		case "RIGHT":
			relativeMoveCaret(1);
			break;
		case "LEFT_WORD":
			moveCaretToPreviousWord();
			break;
		case "RIGHT_WORD":
			moveCaretToNextWord();
		}
		selection.startSelection(anchorPoint);
		selection.updateSelection(caret.getCurrentIndex());
	}

	/**
	 * Colle le contenu du presse-papier à la position du curseur. Si
	 * celui-ci ne contient pas du texte, rien ne sera ajouté.
	 */
	public void pasteClipboard() {

		String text = Clipboard.getSystemClipboard().getString() + "";
		if (text.equals("null")) {
			return;
		}
		insertOnCaret(text);
	}

	/**
	 * Effectue l'action des touches ctrl et C.
	 * Si une sélection est en cours le contenu de celle-ci est placé dans le
	 * presse-papier sinon c'est la ligne correspondant à la position du
	 * curseur qui est placé.
	 */
	public void ctrlCKey() {

		ClipboardContent clipboardContent = new ClipboardContent();
		String           content;

		if (selection.isSelected()) {
			content = textBetween(selection.getStartIndex(),
			                      selection.getEndIndex());
		} else {
			// TODO update currentLine
			int currentLine = indexer.getCurrentLine();
			int indexLine   = indexer.startOfLine(currentLine);
			int endLine     = indexer.endOfLine(currentLine);
			if (currentLine == 1) {
				content = textBetween(indexLine, endLine + 1);
			} else {
				content = textBetween(indexLine, endLine);
			}
		}
		clipboardContent.putString(setEndLineToSystem(content));
		Clipboard.getSystemClipboard().setContent(clipboardContent);
	}

	/**
	 * Effectue l'action des touches Ctrl et X.
	 * <p>
	 * Supprime le contenu de la sélection et le place dans le presse-papier.
	 * S'il n'y a pas de sélection c'est la ligne courante qui est prise en
	 * compte.
	 */
	public void ctrlXKey() {

		ctrlCKey();
		if (selection.isSelected()) {
			relativeDeleteText(0);
		} else {
			int currentLine = indexer.getCurrentLine();
			int indexLine   = indexer.startOfLine(currentLine);
			int endLine     = indexer.endOfLine(currentLine);
			if (currentLine == 1) {
				deleteBetween(indexLine, endLine + 1);
			} else {
				deleteBetween(indexLine, endLine);
			}
		}
	}

	/**
	 * Déplace le curseur au mot suivant.
	 */
	public void moveCaretToNextWord() {

		relativeMoveCaret(offsetNextWord(caret.getCurrentIndex()));
	}

	/**
	 * Déplace le curseur au mot précédent.
	 */
	public void moveCaretToPreviousWord() {

		relativeMoveCaret(offsetPreviousWord(caret.getCurrentIndex()));
	}

	/**
	 * Ajout d'une chaîne de caractères après le curseur.
	 *
	 * @param text la chaîne de caractères
	 */
	public void append(String text) {

		insertOn(indexer.getNumberChar(), text);
	}

	/**
	 * Efface le texte compris entre zéro et le nombre de caractères totale :
	 * cela efface l'ensemble du texte.
	 */
	public void clear() { deleteBetween(0, indexer.getNumberChar()); }

	/** @return the textEditor */
	public TextEditor getTextEditor() { return textEditor; }

	/** @return the indexer */
	public Indexer getIndexer() { return indexer; }

	/**
	 * Obtention du texte entre zéro et le nombre de caractère total : cela
	 * correspond à l'ensemble du texte.
	 *
	 * @return le texte correspondant
	 */
	public String getText() { return textBetween(0, indexer.getNumberChar()); }

	/** @return the textList */
	public ObservableList<Node> getTextList() { return textList; }

	/** @return the numerationText */
	public Text getNumerationText() { return numerationText; }

	/** @return the textFlow */
	public TextFlow getTextFlow() { return textFlow; }

	/** @return the caret */
	public Caret getCaret() { return caret; }

	/** @return the selection */
	public Selection getSelection() { return selection; }

	/** @param readOnly the isReadOnly to set */
	public void setReadOnly(boolean readOnly) { isReadOnly = readOnly; }

	/** Set the color of each Text */
	public void setColorText(Color colorText) {

		textList.forEach(text -> ((Text) text).setFill(colorText));
		textEditor.getPainter().paint();
	}
}