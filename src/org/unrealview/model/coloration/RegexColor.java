/*
 * RegexColor.java               date: 09/02/2020 12:06
 * IUT info1 2019/2020 groupe 3, pas de droits d'auteur
 */

package org.unrealview.model.coloration;

import javafx.scene.paint.Color;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class RegexColor {

	private String regex;
	private Color  color;

	public RegexColor(String regex, Color color) {

		this.regex = regex;
		this.color = color;
	}

	/** @return the regex */
	String getRegex() { return regex; }

	/** @return the color */
	public Color getColor() { return color; }
}