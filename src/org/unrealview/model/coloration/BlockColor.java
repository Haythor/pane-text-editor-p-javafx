/*
 * BlockColor.java               date: 09/02/2020 12:07
 * IUT info1 2019/2020 groupe 3, pas de droits d'auteur
 */

package org.unrealview.model.coloration;

import javafx.scene.paint.Color;

import java.util.Comparator;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
class BlockColor {

	private int   startIndex;
	private int   endIndex;
	private Color color;

	BlockColor(int startIndex, int endIndex, Color color) {

		this.startIndex = startIndex;
		this.endIndex = endIndex;
		this.color = color;
	}

	boolean equals(BlockColor obj) {

		return startIndex == obj.getStartIndex() &&
		       endIndex == obj.getEndIndex() && color == obj.getColor();
	}

	static class Comparators {

		static Comparator<BlockColor> START_INDEX =
				Comparator.comparingInt(BlockColor::getStartIndex);
	}

	/** @return the startIndex */
	int getStartIndex() { return startIndex; }

	/** @return the endIndex */
	int getEndIndex() { return endIndex; }

	/** @param endIndex the endIndex to set */
	void setEndIndex(int endIndex) { this.endIndex = endIndex; }

	/** @return the color */
	Color getColor() { return color; }
}