/*
 * Painter.java                  date: 09/02/2020 12:05
 * IUT info1 2019/2020 groupe 3, pas de droits d'auteur
 */

package org.unrealview.model.coloration;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import org.unrealview.model.text.Indexer;
import org.unrealview.model.text.TextTools;
import org.unrealview.vue.TextEditor;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class Painter {

	private long startTime;

	private TextTools textTools;
	private Indexer   indexer;

	private ArrayList<Node> newList;

	private Color defaultColor;

	private List<RegexColor> regexColorList;
	private List<BlockColor> blockColorList;

	private final Service<Void> computeBlockColor = new Service<>() {

		@Override
		protected void failed() {

			System.err.println("ERROR (computeBlockColor)");
			super.failed();
		}

		@Override
		protected Task<Void> createTask() {

			return new Task<>() {

				@Override
				protected Void call() {

					// startTime = System.currentTimeMillis();

					if (regexColorList.isEmpty()) {
						return null;
					}

					// long time = System.currentTimeMillis();
					makeBlockColor();               // OK
					// System.out.println("Time makeBlockColor : " +
					//                    (System.currentTimeMillis() - time));

					// time = System.currentTimeMillis();
					sortBlock();                    // OK
					// System.out.println("Time sortBlock : " +
					//                    (System.currentTimeMillis() - time));

					// time = System.currentTimeMillis();
					overlapCutting();               // Slow
					// System.out.println("Time overlapCutting : " +
					//                    (System.currentTimeMillis() - time));

					// time = System.currentTimeMillis();
					defaultFillGeneration();        // OK
					// System.out.println("Time defaultFillGeneration : " +
					//                    (System.currentTimeMillis() - time));

					// time = System.currentTimeMillis();
					removeAlreadyApplied();         // OK
					// System.out.println("Time removeAlreadyApplied : " +
					//                    (System.currentTimeMillis() - time));

					// time = System.currentTimeMillis();

					makeNewList();                   // Slow
					// System.out.println("Time makeNewList : " +
					//                    (System.currentTimeMillis() - time));

					// System.out.println("==============END
					// COLORATION==============\n" +
					//                    " ");

					updateTextFlow();

					return null;
				}
			};
		}
	};

	public Painter(TextTools textTools) {

		this.textTools = textTools;
		newList = new ArrayList<>();

		indexer = new Indexer(FXCollections.observableList(newList));

		regexColorList = new ArrayList<>();
		blockColorList = new ArrayList<>();
	}

	/**
	 * TODO describe this method
	 */
	private void makeBlockColor() {

		blockColorList.clear();
		StringBuilder allText = new StringBuilder();
		Matcher       matcher;
		Pattern       pattern;

		for (Node node : newList) {
			allText.append(((Text) node).getText());
		}

		indexer.setNumberChar(allText.length());

		for (RegexColor regexColor : regexColorList) {
			pattern = Pattern.compile(regexColor.getRegex());
			matcher = pattern.matcher(allText);

			while (matcher.find()) {
				BlockColor block = new BlockColor(matcher.start(),
				                                  matcher.end(),
				                                  regexColor.getColor());
				blockColorList.add(block);
				checkPriorityConflict(block);
			}
		}
	}

	/**
	 * TODO describe this method
	 */
	private void checkPriorityConflict(BlockColor block0) {

		int     startRef = block0.getStartIndex();
		int     endRef   = block0.getEndIndex();
		int     start;
		int     end;
		boolean leftOverlap;
		boolean rightOverlap;
		boolean fullMask;

		BlockColor block1;
		for (int i = blockColorList.size() - 2; i >= 0; i--) {
			block1 = blockColorList.get(i);

			start = block1.getStartIndex();
			end = block1.getEndIndex();

			leftOverlap =
					endRef < end && (startRef <= start && start < endRef);
			rightOverlap =
					start < startRef && (startRef < end && end <= endRef);
			fullMask = startRef <= start && end <= endRef;

			if (leftOverlap || rightOverlap || fullMask) {
				blockColorList.remove(block1);
				// i--;
			}
		}
	}

	/**
	 * TODO describe this method
	 */
	private void sortBlock() {

		blockColorList.sort(BlockColor.Comparators.START_INDEX);
		// for (BlockColor blockColor : blockColorList) {
		// 	System.out.print(
		// 			blockColor.getStartIndex() + " " + blockColor.getColor() +
		// 			" " + blockColor.getEndIndex() + " ");
		// } System.out.println();
	}

	/**
	 * TODO describe this method
	 */
	private void overlapCutting() {

		for (int i1 = 0; i1 < blockColorList.size(); i1++) {
			BlockColor block1 = blockColorList.get(i1);

			IntStream.range(0, blockColorList.size()).forEachOrdered(i2 -> {
				BlockColor block2  = blockColorList.get(i2);
				int        b1Start = block1.getStartIndex();
				int        b2Start = block2.getStartIndex();
				int        b1End   = block1.getEndIndex();
				int        b2End   = block2.getEndIndex();
				if (b1Start < b2Start && b2End < b1End) {

					blockColorList.add(i2 + 1,
					                   new BlockColor(b2End,
					                                  b1End,
					                                  block1.getColor()));
					block1.setEndIndex(b2Start);
				}
			});
		}
	}

	/**
	 * TODO describe this method
	 */
	private void defaultFillGeneration() {

		List<BlockColor> newList       = new ArrayList<>();
		int              previousIndex = 0;

		for (BlockColor blockColor : blockColorList) {

			int startIndex = blockColor.getStartIndex();
			if (previousIndex < startIndex) {
				newList.add(new BlockColor(previousIndex,
				                           startIndex,
				                           defaultColor));
			}
			previousIndex = blockColor.getEndIndex();
			newList.add(blockColor);
		}

		if (previousIndex < indexer.getNumberChar()) {
			newList.add(new BlockColor(previousIndex,
			                           indexer.getNumberChar(),
			                           defaultColor));
		}

		blockColorList = newList;
	}

	/**
	 * TODO describe this method
	 */
	private void removeAlreadyApplied() {

		int countChar = 0;

		for (Node text : newList) {
			int lengthText = ((Text) text).getText().length();
			BlockColor textBlockColor = new BlockColor(countChar,
			                                           countChar + lengthText,
			                                           (Color) ((Text) text).getFill());
			blockColorList.stream()
			              .filter(textBlockColor::equals)
			              .findFirst()
			              .ifPresent(blockColor -> blockColorList.remove(
					              blockColor));
			countChar += lengthText;
		}
	}

	/**
	 * TODO describe this method
	 */
	private void makeNewList() {

		for (BlockColor blockColor : blockColorList) {

			indexer.refreshIndexText();

			int[] startLocal = indexer.localIndex(blockColor.getStartIndex());
			int[] endLocal = indexer.localIndex(blockColor.getEndIndex() - 1);

			int start = startLocal[0];
			int end   = endLocal[0];
			if (end - start == 1) {
				connect(end - 1, end);
			} else {
				for (int i = start + 1; i <= end; end--) {
					connect(i - 1, i);
				}
			}

			cut(((Text) newList.get(startLocal[0])),
			    blockColor.getStartIndex() - startLocal[1]);

			cut(((Text) newList.get(startLocal[0])),
			    blockColor.getEndIndex() - startLocal[1]);


			Text text = (Text) newList.get(startLocal[0]);
			text.setFill(blockColor.getColor());
			text.setFont(TextEditor.getFONT());
		}

		// for (BlockColor blockColor : blockColorList) {
		// 	System.out.print(
		// 			blockColor.getStartIndex() + " " + blockColor.getColor() +
		// 			" " + blockColor.getEndIndex() + " ");
		// }
		// System.out.println();
		// System.out.println("newList.size() = " + newList.size());
	}

	/**
	 * TODO describe this method
	 */
	private void updateTextFlow() {

		// long time = System.currentTimeMillis();

		if (!blockColorList.isEmpty()) {

			Platform.runLater(() -> {

				textTools.getTextFlow().getChildren().clear();
				textTools.getTextFlow().getChildren().addAll(newList);
				textTools.getIndexer().getIndexesLine().restart();
				textTools.getIndexer().getIndexesText().restart();

				// System.out.println("Time updateTextFlow : " +
				//                    (System.currentTimeMillis() - time));
			});
		}


		// System.out.println("TOTAL TIME PAINT : " +
		//                    (System.currentTimeMillis() - startTime));
		// System.out.println();
	}

	/**
	 * TODO describe this method
	 */
	private void cut(Text text, int index) {

		// System.out.println("\nCUT METHOD");
		// System.out.println("text.getText() = '" + text.getText() + "'");
		// System.out.println("index = " + index);
		String content = text.getText();
		if (content.isEmpty() || index == 0 || index > content.length() - 1) {
			return;
		}

		Text newText   = new Text(content.substring(index));
		int  indexText = newList.indexOf(text);

		text.setText(content.substring(0, index));

		// System.out.println("text.getText() = " + text.getText());
		// System.out.println("newText.getText() = " + newText.getText());

		if (!newText.getText().isEmpty()) {
			newList.add(indexText + 1, newText);
		}
		if (text.getText().isEmpty()) {
			newList.remove(text);
		}
	}

	/**
	 * TODO describe this method
	 */
	private void connect(int leftText, int rightText) {

		// System.out.println("\nCONNECT METHOD");

		Text lText = (Text) newList.get(leftText);
		Text rText = (Text) newList.get(rightText);

		// System.out.println("leftText.getText() = " + lText.getText());
		// System.out.println("rightText.getText() = " + rText.getText());

		lText.setText(lText.getText() + rText.getText());
		newList.remove(rText);
	}

	/**
	 * TODO describe this method
	 */
	public void paint() {

		newList.clear();
		for (Node node : textTools.getTextList()) {
			Text text = new Text(((Text) node).getText());
			text.setFill(((Text) node).getFill());
			text.setFont(((Text) node).getFont());
			newList.add(text);
		}
		computeBlockColor.restart();
	}

	/**
	 * TODO describe this method
	 */
	public void addRegexColor(RegexColor regexColor) {

		regexColorList.add(regexColor);
	}

	/**
	 * TODO describe this method
	 */
	public void removeRegexColor(String regex) {

		regexColorList.stream()
		              .filter(regexColor -> regexColor.getRegex().equals(regex))
		              .findFirst()
		              .ifPresent(regexColor -> regexColorList.remove(regexColor));
	}

	/** @param defaultColor the defaultFill to set */
	public void setDefaultColor(Color defaultColor) {

		this.defaultColor = defaultColor;
	}
}