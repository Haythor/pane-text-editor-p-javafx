/*
 * Caret.java                    date: 29/01/2020 08:18
 * IUT info1 2019/2020 groupe 3, pas de droits d'auteur
 */

package org.unrealview.model;

import javafx.animation.FadeTransition;
import javafx.animation.PauseTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.scene.paint.Color;
import javafx.scene.shape.Path;
import javafx.scene.shape.PathElement;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.TextFlow;
import javafx.util.Duration;
import org.unrealview.model.text.TextTools;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class Caret {

	private final double TIME_ANIMATION  = 80;
	private final double PAUSE_ANIMATION = 180;
	private final double PAUSE_PATH      = 18;
	private final double AUTO_SCROLL     = 40;

	private SequentialTransition seqTransition;
	private FadeTransition       ft;
	private PauseTransition      pause1;
	private PauseTransition      pause2;
	private PauseTransition      pausePathRequest;
	private PauseTransition      pauseAutoScroll;

	private TextTools textTools;
	private TextFlow  textFlow;

	private Path caretPath;

	private Rectangle activeLineText;
	private Rectangle activeLineTextFit;
	private Rectangle activeLineNumber;

	private int currentIndex;

	public Caret(TextTools textTools) {

		this.textTools = textTools;

		textFlow = textTools.getTextFlow();

		caretPath = new Path();
		caretPath.setStrokeWidth(1);
		caretPath.setSmooth(false);
		caretPath.layoutXProperty().bind(textFlow.layoutXProperty());
		caretPath.layoutYProperty().bind(textFlow.layoutYProperty().add(-3));

		activeLineText = new Rectangle();
		activeLineTextFit = new Rectangle();
		activeLineNumber = new Rectangle();

		pause1 = new PauseTransition(Duration.millis(PAUSE_ANIMATION));
		ft = new FadeTransition(Duration.millis(TIME_ANIMATION), caretPath);
		ft.setFromValue(1);
		ft.setToValue(0);
		pause2 = new PauseTransition(Duration.millis(PAUSE_ANIMATION));

		seqTransition = new SequentialTransition(pause1, ft, pause2);
		seqTransition.setCycleCount(Timeline.INDEFINITE);
		seqTransition.setAutoReverse(true);

		pausePathRequest = new PauseTransition(Duration.millis(PAUSE_PATH));
		pausePathRequest.setOnFinished(e -> {
			PathElement[] caretPathElement =
					textFlow.caretShape(currentIndex, true);
			caretPath.getElements().clear();
			caretPath.getElements()
			         .addAll(caretPathElement[0], caretPathElement[1]);
			activeLineRefresh();
			caretPath.setOpacity(1);
			seqTransition.playFromStart();
		});

		pauseAutoScroll =
				new PauseTransition(Duration.millis(AUTO_SCROLL));
		pauseAutoScroll.setOnFinished(e-> textTools.getTextEditor().autoScroll());
	}

	/**
	 * Actualisation de la position des rectangles dessinant la ligne active.
	 */
	private void activeLineRefresh() {

		double heightCaret  = caretPath.getLayoutBounds().getHeight();
		double layoutYCaret = caretPath.getLayoutBounds().getCenterY();

		activeLineText.setY(layoutYCaret);
		activeLineTextFit.setY(layoutYCaret);
		activeLineNumber.setY(layoutYCaret);

		activeLineText.setHeight(heightCaret);
		activeLineTextFit.setHeight(heightCaret);
		activeLineNumber.setHeight(heightCaret);
	}

	/**
	 * Arrête la séquence d'animation du curseur et fait disparaître celui-ci.
	 */
	public void focusLost() {

		seqTransition.stop();
		caretPath.setOpacity(0);
	}

	/**
	 * Reprends la séquence d'animation du curseur.
	 */
	public void focusAcquired() {

		seqTransition.play();
	}

	/**
	 * Effectue un déplacement du curseur selon offset par rapport à sa
	 * position actuelle.
	 *
	 * @param offset nombre de caractères de déplacement
	 */
	public void relativeMoveCaret(int offset) {

		setIndexCaret(currentIndex + offset, false);
	}

	/**
	 * Positionne le curseur à un index donné.
	 *
	 * @param charIndex Un index par rapport à l'ensemble du texte (s'il
	 *                  dépasse les limites il est défini au plus proche)
	 * @param isSlow    si vrai, un temps d'attente sera appliqué avant de
	 *                  positionner le curseur
	 */
	public void setIndexCaret(int charIndex, boolean isSlow) {

		currentIndex = Math.max(0,
		                        Math.min(charIndex,
		                                 textTools.getIndexer()
		                                          .getNumberChar()));
		if (isSlow) {
			pausePathRequest.playFromStart();
		} else {
			pausePathRequest.playFrom(Duration.millis(PAUSE_PATH));
		}
		textTools.getIndexer().setCurrentIndex(currentIndex);
		pauseAutoScroll.play();
	}

	/** @return the currentIndex */
	public int getCurrentIndex() { return currentIndex; }

	/** @return the activeLineText */
	public Rectangle getActiveLineText() { return activeLineText; }

	/** @return the activeLineNumber */
	public Rectangle getActiveLineNumber() { return activeLineNumber; }

	/** @return the activeLineTextFit */
	public Rectangle getActiveLineTextFit() { return activeLineTextFit; }

	/** @return the caretPath */
	public Path getCaretPath() { return caretPath; }

	/**
	 * Paramètres la couleur du curseur.
	 *
	 * @param colorCaret la nouvelle couleur
	 */
	public void setColorCaret(Color colorCaret) {

		caretPath.setStroke(colorCaret);
	}

	/**
	 * Paramètres la couleur de l'effet de ligne active.
	 *
	 * @param colorActive la nouvelle couleur
	 */
	public void setColorActive(Color colorActive) {

		activeLineNumber.setFill(colorActive);
		activeLineText.setFill(colorActive);
		activeLineTextFit.setFill(colorActive);
	}
}