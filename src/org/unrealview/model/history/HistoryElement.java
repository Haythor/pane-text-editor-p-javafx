/*
 * HistoryElement.java           date: 09/02/2020 14:33
 * IUT info1 2019/2020 groupe 3, pas de droits d'auteur
 */

package org.unrealview.model.history;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
class HistoryElement {

	private String id;

	HistoryElement(String id) { this.id = id; }

	/** @return the id */
	String getId() { return id; }
}