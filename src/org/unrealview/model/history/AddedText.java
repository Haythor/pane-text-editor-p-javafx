/*
 * AddedText.java                date: 09/02/2020 12:14
 * IUT info1 2019/2020 groupe 3, pas de droits d'auteur
 */

package org.unrealview.model.history;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class AddedText extends HistoryElement {

	private int startIndex;
	private int endIndex;

	public AddedText(int startIndex, int endIndex) {

		super("add");
		this.startIndex = startIndex;
		this.endIndex = endIndex;
	}

	/** @return the startIndex */
	int getStartIndex() { return startIndex; }

	/** @return the endIndex */
	int getEndIndex() { return endIndex; }
}