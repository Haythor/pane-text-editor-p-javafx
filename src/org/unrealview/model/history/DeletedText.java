/*
 * DeletedText.java              date: 09/02/2020 12:14
 * IUT info1 2019/2020 groupe 3, pas de droits d'auteur
 */

package org.unrealview.model.history;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class DeletedText extends HistoryElement {

	private String text;
	private int    startIndex;

	public DeletedText(String text, int startIndex) {

		super("delete");
		this.text = text;
		this.startIndex = startIndex;
	}

	/** @return the text */
	String getText() { return text; }

	/** @return the startIndex */
	int getStartIndex() { return startIndex; }
}