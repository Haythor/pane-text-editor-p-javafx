/*
 * History.java                  date: 09/02/2020 12:08
 * IUT info1 2019/2020 groupe 3, pas de droits d'auteur
 */

package org.unrealview.model.history;

import org.unrealview.model.text.TextTools;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class History {

	private TextTools textTools;

	private List<HistoryElement> listOfUndo;
	private List<HistoryElement> listOfRedo;
	private boolean              isRetroAction;

	public History(TextTools textTools) {

		this.textTools = textTools;
		listOfUndo = new ArrayList<>();
		listOfRedo = new ArrayList<>();
		isRetroAction = false;
	}

	/**
	 * TODO describe this method
	 */
	private void executeAction(HistoryElement historyElement) {

		switch (historyElement.getId()) {
		case "delete":
			DeletedText deletedText = (DeletedText) historyElement; textTools.insertOn(
				deletedText.getStartIndex(),
				deletedText.getText());
			break;
		case "add":
			AddedText addedText = (AddedText) historyElement; textTools.deleteBetween(
				addedText.getStartIndex(),
				addedText.getEndIndex());
		}
	}

	/**
	 * TODO describe this method
	 */
	public void undo() {

		int numberOfElement = listOfUndo.size();
		if (numberOfElement == 0) {
			return;
		}
		isRetroAction = true;
		HistoryElement currentElement = listOfUndo.get(numberOfElement - 1);
		executeAction(currentElement);
		listOfUndo.remove(currentElement);
	}

	/**
	 * TODO describe this method
	 */
	public void redo() {

		int numberOfElement = listOfRedo.size();
		if (numberOfElement == 0) {
			return;
		}
		HistoryElement currentElement = listOfRedo.get(numberOfElement - 1);
		executeAction(currentElement);
		listOfRedo.remove(currentElement);
	}

	/**
	 * TODO describe this method
	 */
	public void addElement(HistoryElement historyElement) {

		if (isRetroAction) {
			listOfRedo.add(historyElement);
			isRetroAction = false;
		} else {
			listOfUndo.add(historyElement);
		}
	}

	/**
	 * TODO describe this method
	 */
	public void clear() {

		listOfRedo.clear();
		listOfUndo.clear();
	}
}